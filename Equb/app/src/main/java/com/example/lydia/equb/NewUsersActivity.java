package com.example.lydia.equb;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NewUsersActivity extends AppCompatActivity {

    DatabaseHelper dh;
    EditText NameEditText;
    ListView MemberList, AmountList;
    ArrayList<String> members, amounts;
    ArrayAdapter membersAdapter, amountsAdapter;
    int i = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_users);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMember();
            }
        });


        MemberList = findViewById(R.id.member_list);
        AmountList = findViewById(R.id.amount_list);
        members = new ArrayList<String>();
        membersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,members);
        MemberList.setAdapter(membersAdapter);

        amounts = new ArrayList<String>();
        amountsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,amounts);
        AmountList.setAdapter(amountsAdapter);

        dh = new DatabaseHelper(this);
        NameEditText = findViewById(R.id.name_edit_text);



        viewMembers();
    }

    private void addMember() {
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(50, 20, 50, 20);

        TextView NameTextView = new TextView(this);
        NameTextView.setText("Name:");
        linearLayout.addView(NameTextView);

        final EditText NameEditText = new EditText(this); // capitalize
        NameEditText.setFocusable(true);
        linearLayout.addView(NameEditText);

        TextView AmountTextView = new TextView(this);
        AmountTextView.setText("Amount:");
        linearLayout.addView(AmountTextView);

        final EditText AmountEditText = new EditText(this);
        linearLayout.addView(AmountEditText);

        AlertDialog.Builder ad=new AlertDialog.Builder(this);
        ad.setView(linearLayout);

        ad.setMessage("Add Member");

        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                    i++;

                    dh.addMember(i, NameEditText.getText().toString(), AmountEditText.getText().toString());

                    members.add(NameEditText.getText().toString());
                    membersAdapter.notifyDataSetChanged();

                    amounts.add(AmountEditText.getText().toString());
                    amountsAdapter.notifyDataSetChanged();
            }
        });
        ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        ad.show();
    }



    private void createEqub() {
        if(dh.addEqub(NameEditText.getText().toString(), 0)){
            Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show();
        }
    }

    public void viewMembers(){
        Cursor cursor = dh.getMember();

        if(cursor.moveToFirst()){
            String[] member = new String[cursor.getCount()];
            String[] amount = new String[cursor.getCount()];
            int i = 0;
            do{
                member[i] = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME));
                amount[i] = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_AMOUNT)) + "";
                i++;
            }while(cursor.moveToNext());

            ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, member);
            MemberList.setAdapter(adapter);
            ArrayAdapter adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, amount);
            AmountList.setAdapter(adapter2);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (i == 0){
            AlertDialog.Builder ad=new AlertDialog.Builder(this);
            ad.setMessage("There are no Members in the Equb");
            ad.setCancelable(true);
            ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                }
            });

            ad.show();
        }else {
            createEqub();
            Intent intent = new Intent(NewUsersActivity.this, EqubActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
