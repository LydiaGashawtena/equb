package com.example.lydia.equb;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class EqubActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DatabaseHelper dh;
    Cursor MembersCursor;
    TableLayout MembersTable;
    String member, amount, paid, win, EqubName, Winner = "";
    int id, Total, MonthlyTotal, CurrentMonth, WinnerId, WinnerAmount;
    TextView TotalTextView, WinnerTextView;
    ArrayList<Integer> ListOfMembers = new ArrayList<Integer>();
    ArrayList<TextView> ListOfButtons = new ArrayList<>();
    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equb);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        MembersTable = findViewById(R.id.members_table);
        TotalTextView = findViewById(R.id.total_text_view);
        WinnerTextView = findViewById(R.id.winner_text_view);
        dh = new DatabaseHelper(this);
        MembersCursor = dh.getMember();

        getEqubData();
        viewMembers();
        setWinner(MonthlyTotal);


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void viewMembers(){
        TotalTextView.append(Total+"");

        if(MembersCursor.moveToFirst()){


            do{
                id = MembersCursor.getInt(MembersCursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                member = MembersCursor.getString(MembersCursor.getColumnIndex(DatabaseHelper.COLUMN_NAME));
                amount = MembersCursor.getInt(MembersCursor.getColumnIndex(DatabaseHelper.COLUMN_AMOUNT)) + "";
                paid = MembersCursor.getString(MembersCursor.getColumnIndex(DatabaseHelper.COLUMN_PAID));
                win = MembersCursor.getString(MembersCursor.getColumnIndex(DatabaseHelper.COLUMN_WIN));
                ListOfMembers.add(id);
                MonthlyTotal += Integer.parseInt(amount);

                if(win.equals("isWinner")){
                    WinnerId = id;
                    WinnerTextView.setText(getString(R.string.month_winner) + member);
                }

                TableRow row = new TableRow(this);
                row.setPadding(0, 30, 0, 30);


                TextView memberCol = new TextView(this);
                memberCol.setText(member);
                //Toast.makeText(this, id, Toast.LENGTH_LONG).show();
                memberCol.setTextColor(Color.BLACK);
                memberCol.setTextSize(17);
                memberCol.setPadding(40, 5, 170, 10);
                row.addView(memberCol);

                TextView amountCol = new TextView(this);
                amountCol.setText(amount);
                //amountCol.setTextColor(Color.BLACK);
                amountCol.setTextSize(17);
                amountCol.setPadding(40, 5, 250, 10);
                row.addView(amountCol);

                TextView paidButton = new TextView(this);
                paidButton.setText("      ");
                if (Objects.equals(paid, "true")){
                    paidButton.setBackgroundColor(Color.GREEN);
                    //Toast.makeText(this, member, Toast.LENGTH_LONG).show();
                }else{
                    paidButton.setBackgroundColor(Color.RED);
                }

                paidButton.setOnClickListener(new ButtonHandler(id+"", paidButton, amount));
                row.addView(paidButton);
                ListOfButtons.add(paidButton);

                MembersTable.addView(row);


            }while(MembersCursor.moveToNext());

        }

    }

    public class ButtonHandler implements View.OnClickListener{
        String id;
        TextView paidButton;
        int CurrentAmount;

        ButtonHandler(String id, TextView paidButton, String CurrentAmount){
            this.id = id;
            this.paidButton = paidButton;
            this.CurrentAmount = Integer.parseInt(CurrentAmount);
        }


        @Override
        public void onClick(View v) {
            dh.update("paid", "true", id);
            Total += this.CurrentAmount;
            dh.update("total", Total+"", EqubName);
            paidButton.setBackgroundColor(Color.GREEN);
            TotalTextView.setText(getString(R.string.totalText) + Total);
        }
    }

    public void getEqubData(){
        Cursor EqubCursor = dh.getEqub();

        if(EqubCursor.moveToFirst()){
            do{
                Total = EqubCursor.getInt(EqubCursor.getColumnIndex(DatabaseHelper.COLUMN_TOTAL));
                EqubName = EqubCursor.getString(EqubCursor.getColumnIndex(DatabaseHelper.COLUMN_ENAME));
                CurrentMonth = EqubCursor.getInt(EqubCursor.getColumnIndex(DatabaseHelper.COLUMN_CURRENT_MONTH));

                this.setTitle(months[CurrentMonth]);
            }while(EqubCursor.moveToNext());
        }

    }


    public void setWinner(int CompareTo) {
        int i = 0;
        String WinStatus = "";
        int Amount = 0;
        String name = "";
        while (i <= ListOfMembers.size()) {
            Random r = new Random();
            int WinnerId = ListOfMembers.get(r.nextInt(ListOfMembers.size()));
            Cursor WinnerData = dh.getMemberData(WinnerId);
            if(WinnerData.moveToFirst()) {
                do {
                    name = WinnerData.getString(WinnerData.getColumnIndex(DatabaseHelper.COLUMN_NAME));
                    WinStatus = WinnerData.getString(WinnerData.getColumnIndex(DatabaseHelper.COLUMN_WIN));
                    Amount = WinnerData.getInt(WinnerData.getColumnIndex(DatabaseHelper.COLUMN_AMOUNT));
                } while (WinnerData.moveToNext());
            }
            if (WinStatus.equals("notWon") && (Amount * 12) <= CompareTo) {
                dh.update("win", "isWinner", WinnerId + "");
                Winner = name;
                WinnerAmount = Amount * 12;
                break;
            }else{
                Winner = "none";
                WinnerAmount = 0;
            }
            i++;
        }
        WinnerTextView.setText(getString(R.string.month_winner) + Winner);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.equb_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        CurrentMonth++;
        if (CurrentMonth == 12){
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(EqubActivity.this);
            alertDialog.setMessage(R.string.finished);
            alertDialog.setCancelable(true);
            alertDialog.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dh.deleteEqub();
                    Intent intent = new Intent(EqubActivity.this, NewUsersActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            alertDialog.show();
        }else {
            Total -= WinnerAmount;
            dh.update("total", Total + "", EqubName);
            TotalTextView.setText(getString(R.string.totalText) + Total + "");
            Toast.makeText(EqubActivity.this, WinnerAmount + "", Toast.LENGTH_SHORT).show();
            dh.update("win", "hasWon", Winner);
            dh.update("month", CurrentMonth + "", EqubName);
            for (int i = 0; i < ListOfButtons.size(); i++) {
                ListOfButtons.get(i).setBackgroundColor(Color.RED);
                dh.update("paid", "false", ListOfMembers.get(i) + "");
            }
            this.setTitle(months[CurrentMonth]);
            setWinner(Total + MonthlyTotal);

        }
        return super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_create) {
            dh.deleteEqub();
            finish();
            Intent intent = new Intent(EqubActivity.this, NewUsersActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(EqubActivity.this, AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_close) {
            finish();
            moveTaskToBack(true);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
