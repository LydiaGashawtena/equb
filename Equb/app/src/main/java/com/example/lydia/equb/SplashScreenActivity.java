package com.example.lydia.equb;

import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    Handler handler;
    DatabaseHelper dh;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        dh = new DatabaseHelper(this);
        cursor = dh.getEqub();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               if(cursor.getCount() == 0) {
                    Intent intent = new Intent(SplashScreenActivity.this, NewUsersActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(SplashScreenActivity.this, EqubActivity.class);
                    startActivity(intent);
                }
                /*Intent intent = new Intent(SplashScreenActivity.this, EqubActivity.class);
                startActivity(intent);*/

                finish();
            }
        }, 3000);


    }
}
