package com.example.lydia.equb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;

/**
 * Created by Lydia on 5/24/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    public static final String DB_NAME = "mySQLiteDatabase";
    public static final String EQUB_TABLE = "equb";
    public static final String COLUMN_ENAME = "name";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_CURRENT_MONTH = "month";
    public static final String MEMBER_TABLE = "member";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_PAID = "paid";
    public static final String COLUMN_WIN = "win";

    public static final int DB_VERSION = 8;

    public static  String Member_table_create_query = "";

    public DatabaseHelper(Context context){
        super(context,DB_NAME,null,DB_VERSION);

    }

    @Override

    public void onCreate(SQLiteDatabase db) {
        String Equb_table_create_query = "create table " + EQUB_TABLE + "(" + COLUMN_ENAME + " varchar, " + COLUMN_TOTAL + " integer, " +
                COLUMN_CURRENT_MONTH + " integer default 0)";
        Member_table_create_query = "create table " + MEMBER_TABLE + "(" + COLUMN_ID + " integer primary key, " + COLUMN_NAME + " varchar, " + COLUMN_AMOUNT + " integer, " +
                COLUMN_PAID + " varchar, " + COLUMN_WIN + " varchar)";
        db.execSQL(Equb_table_create_query);
        db.execSQL(Member_table_create_query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String equb_table_update_query = "drop table if exists " + EQUB_TABLE;
        String member_table_update_query = "drop table if exists " + MEMBER_TABLE;
        db.execSQL(equb_table_update_query);
        db.execSQL(member_table_update_query);
        onCreate(db);
        //db.execSQL(Member_table_create_query);


    }

    public boolean addEqub(String name, int total){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ENAME, name);
        cv.put(COLUMN_TOTAL, total);

        return db.insert(EQUB_TABLE, null, cv) != -1;
    }

    public boolean addMember(int id, String name, String amount){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, id);
        cv.put(COLUMN_NAME, name);
        cv.put(COLUMN_AMOUNT, amount);
        cv.put(COLUMN_PAID, "false");
        cv.put(COLUMN_WIN, "notWon");

        return db.insert(MEMBER_TABLE, null, cv) != -1;
    }

    public Cursor getEqub(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + EQUB_TABLE + ";";
        return db.rawQuery(query, null);
    }

    public Cursor getMember(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + MEMBER_TABLE + ";";
        return db.rawQuery(query, null);
    }

    public int deleteEqub(){
        SQLiteDatabase db = this.getWritableDatabase();
        int deletedEqub = db.delete(EQUB_TABLE, null, null);
        int deleted = db.delete(MEMBER_TABLE, null, null);
        return deleted;
    }

    public void update(String action, String newValue, String where){
        SQLiteDatabase db = this.getWritableDatabase();
        if (action.equals("paid")){
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_PAID, newValue);
            db.update(MEMBER_TABLE, cv, COLUMN_ID + " =? ", new String[]{where});
        }

        if (action.equals("total")){
            ContentValues cv = new ContentValues();
            int value = Integer.parseInt(newValue);
            cv.put(COLUMN_TOTAL, value);
            db.update(EQUB_TABLE, cv, COLUMN_ENAME+ " =? ", new String[]{where});
        }

        if (action.equals("win")){
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_WIN, newValue);
            db.update(MEMBER_TABLE, cv, COLUMN_ID+ " =? ", new String[]{where});
        }

        if (action.equals("month")){
            ContentValues cv = new ContentValues();
            int value = Integer.parseInt(newValue);
            cv.put(COLUMN_CURRENT_MONTH, value);
            db.update(EQUB_TABLE, cv, COLUMN_ENAME+ " =? ", new String[]{where});
        }

    }

    public Cursor getMemberData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + MEMBER_TABLE + " where "+ COLUMN_ID + " = " + id +";";
        return db.rawQuery(query, null);
    }



}

